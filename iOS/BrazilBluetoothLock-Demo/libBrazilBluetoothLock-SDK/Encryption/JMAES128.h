//
//  JMAES128.h
//  MengGouSharedLocksDemo
//
//  Created by Smile on 2017/11/14.
//  Copyright © 2017年 Smile. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 根据设备对数据进行加密,解密
 因为发给设备,以及设备返回的数据,加密解密是不一样的,所以区分写开
 */
@interface JMAES128 : NSObject

/**
 加密
 
 @param string 需要加密的string
 @return 加密后的字符串
 */
+(NSString *)AES128EncryptStrig:(NSString *)string;


/**
 解密
 
 @param string 解需要密的字符串
 @return 解密后的内容
 */
+(NSString *)AES128DecryptString:(NSString *)string;

@end
