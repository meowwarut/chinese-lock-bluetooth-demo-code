//
//  AppDelegate.h
//  BrazilBluetoothLock-Demo
//
//  Created by Smile on 2017/12/28.
//  Copyright © 2017年 com.shenzhen.jimi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

