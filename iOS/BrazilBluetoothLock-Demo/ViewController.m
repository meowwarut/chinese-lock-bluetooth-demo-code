//
//  ViewController.m
//  BrazilBluetooth-SDK
//
//  Created by Smile on 2017/12/26.
//  Copyright © 2017年 com.shenzhen.jimi. All rights reserved.
//

#import "ViewController.h"
#import "JMBluetoothTooth.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *macAddressTextField;
@property (weak, nonatomic) IBOutlet UITextView *logInfoTextView;

@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.macAddressTextField.text = @"C4A828082B59";
    self.logInfoTextView.text = @"";
    [self.logInfoTextView setEditable:NO];
    
    //记得移除监听通知 Remember to remove the listener notifications
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(closeLock:) name:kCloseLockNotificationName object:nil];
}

//关锁 Close Lock
-(void)closeLock:(NSNotification *)sender{
    
    NSLog(@"sender:%@", sender);
    NSDictionary *userInfo = sender.userInfo;
    NSNumber *res = (NSNumber *)userInfo[@"info"];
    
    if ([res intValue] == 0){
        [self updateUI:@"Close Lock success" andError:nil];
    }else{
        [self updateUI:@"Close Lock failure" andError:nil];
    }
}

- (IBAction)connectionBtnClick:(UIButton *)sender {
    
    //1.赋值设备名称
    kJMBluetoothManager.jmDeviceMacAddress = self.macAddressTextField.text;
    
    //2.开始扫描外设, 执行连接
    __weak typeof(self) weakSelf = self;
    [kJMBluetoothManager JMBluetoothStartConnectionWithWidthDeviceMacAddress:@"" ConnectBlock:^(JMOperationResult result, CBPeripheral * _Nonnull peripheral, NSError * _Nullable error) {
        
        if (error == nil){
            [weakSelf updateUI:@"Connection success" andError:nil];
        }else{
            [weakSelf updateUI:@"connection failed" andError:error];
        }
    }];
    
    kJMBluetoothManager.jmBluetoothGetTokenWithBlock = ^(NSString * _Nonnull token, NSError * _Nonnull error) {
        
        if (error == nil){
            [weakSelf updateUI:[NSString stringWithFormat:@"Get Token success:%@",token] andError:nil];
        }else{
            [weakSelf updateUI:@"Get Token failed" andError:error];
        }
    };
}


//开锁按钮点击事件
- (IBAction)openLockBtnClick:(UIButton *)sender {
    
     __weak typeof(self) weakSelf = self;
    [kJMBluetoothManager JMOpenLockWidthCallback:^(JMOperationResult result, NSArray * _Nonnull message, NSError * _Nullable error) {
        if (error == nil){
            [weakSelf updateUI:@"Unlock success" andError:nil];
        }else{
            [weakSelf updateUI:@"success failed" andError:error];
        }
    }];
}

//断开连接
- (IBAction)disconnectBtnClick:(UIButton *)sender{
    [kJMBluetoothManager JMDisconnect];
    [self updateUI:@"The connection has been disconnected" andError:nil];
}


/** 清空loc输出 */
- (IBAction)clearLogInfo:(UIButton *)sender {
    self.logInfoTextView.text = @"";
}

-(void)updateUI:(NSString *)msg andError:(NSError *)error{
    NSString *str = @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss:SSS"];
    NSString *currentDateStr = [dateFormatter stringFromDate:[NSDate date]];
    
    if (error == nil){
        str = [str stringByAppendingString:[NSString stringWithFormat:@"\n%@_%@",currentDateStr, msg]];
    }else{
        str = [NSString stringWithFormat:@"\n%@_%@\nerrorMsg:%@",currentDateStr, msg, error];
    }
    
    //通知主线程刷新
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *temp = self.logInfoTextView.text;
        temp = [temp stringByAppendingString:str];
        [self.logInfoTextView setText:temp];
    });
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view resignFirstResponder];
}

@end

















